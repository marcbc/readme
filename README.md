This page was inspired by [Kenny Johnston's README](https://gitlab.com/kencjohnston/README)

## Nicole's readme
My name is Nicole Schwartz and I'm the Product Manager, Secure at GitLab.

[GitLab](https://gitlab.com/NicoleSchwartz)

If we are communicating here are some things it might be helpful to know about me:

### General

*  My strengths are documenting and organzing.
    *    I have a wekaness around providing enough granular detail, please let me know if I have not been clear or detailed enough!
*  I tend to jump in where I see things needing some help whenever I can.
*  I type too fast and make spelling mistakes where there isn't a native spell checker.
*  I tend to be hyperbolic when brainstorming, ask me if you aren't sure.
*  I swear sometimes. Let me know if you mind and I will be extra dilligent to avoid it.
*  The more excited I get the louder and faster I talk, and it tend to include arm waving.
*  I believe in [disagree, and commit](https://en.wikipedia.org/wiki/Disagree_and_commit).
*  I love to argue about ideas or things, but let's avoid people.
    *    If you don't like to argue please let me know and I will take a different aproach of exploring ideas when working with you.
*  I often need to talk or roll ideas around with other people before they are fully formed. When I ask for comments or feedback on an issue feel free to rip the idea to shreds, constructively. 
*  Outside of work I love to to travel, play games (RPG, board, video, console, mobile...), and anything to do with tea.
*  I have a cat, Slacks, and a rabbit, Pepper.

### Product

*  I enjoy being a [Product Manager](https://about.gitlab.com/job-families/product/product-manager/) because it allows me to interact with customers, developers, designers, testers, and more. I love being able to help build a vision, get everyone on the same page, act as a subject matter expert on my area, and then have the ability to dive into designing or improving feature specifics.
    *    You can read more about good project management practices at [Silicon Valley Product Group - SVPG](https://svpg.com/)
*  I have been involved in the hacker scene since I was young, and before there was an Information Security industry. I am very excited to be involved in it as both a passion and a career.

### Logistics

*  When you need to reach me, here are my communication priorities and preference:
    *    Slack - If I'm working, I'm on slack. I do not have sounds or alerts enabled. I may not be paying attention but I will check this most often. @ me into a thread so I can read it, or direct message me.
    *    Email - I check email once a day, if that. I do not have sounds or alerts enabled.
    *    SMS - Emergencies only please. Outside of work hours I'm hit or miss on SMS.
    *    Phone - I am always in do-not-disturb mode and will not answer the phone unless I am expecting your call.
  * I prefer to avoid email and SMS for the following activities:
    *    Items requiring discussion - better to create an issue, start a conversation on slack or setup a meeting
*  I consider Slack, Email, and SMS to be asynchronous. If it needs to be synchronous please setup or request a meeting.
*  If there is a timeline for a response needed, please specificy it. For example - "Hey, can you review the issue before our meeting tomorrow?"
*  I might message you when you are not working (off hours), on vacation, or on a weekend or holiday. You can absolutely respond when you want, I am just trying to get it out of my head and over to you when an idea finishes forming.

### Teams I Support

*  I support the [Secure Stages](https://about.gitlab.com/stages-devops-lifecycle/secure/).

### Feedback

* Please provide feedback to me, in private, and specifically indicate it is feedback. It will work best if you are clear and specific.
* Is there something I should include here to help you communicate with me? Submit a merge request.
