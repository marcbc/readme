This page is to share my office setup.

## Both at home and on the road

 - Apple 13" McBook Pro 13.3/2.3GHz/16GB/256GB/KB-US 2019-05-01
 - Bose QuietComfort 35 II Bluetooth wireless headphones with microphone $449.99CAD 2019-05-01
 - logitech USB mouse $4USD 2019-05-01
 - mousepad $5USD 2019-05-01
 - j5 create USB type-c multi adapter (ethernet, HDMI, 3 USB, SD, mini SD, and power) warning runs very hot purchased may we'll see how long it lasts $79.99USD 2019-05-01
 - 3 cases for the laptop, so i can hold OPSEC when needed
   - [CASETiFY / MacBook Sleeves / MacBook Pro 13 Cases / Black white modern chic](https://www.casetify.com/product/VuAer_black-white-modern-chic-marble-texture-patterns/macbook13/macbook-pro-13-inch-snap-case#/283700) generic cover 2019-05-01
   - 2 clear cases similar to above from eBay to cover in stickers (1 Gitlab 1 Hacker) 2019-05-01
 - [The roost stand](https://www.therooststand.com/products/roost-laptop-stand?gclid=EAIaIQobChMI-5vWkMbG4gIVSUwNCh2WiQAvEAAYASAAEgK78vD_BwE) ~$80USD 2019-05-01
 - [Anker PowerPort Strip 3 with 3 USB Ports](https://www.anker.com/products/variant/powerport-strip-3-with-3-usb-ports/A2763121) ~$30USD 2019-05-01

## At home

 - [Ikea BEKANT Sit/stand underframe/corner table, 63x43 1/4 " (160x110 cm)](https://www.ikea.com/ca/en/p/bekant-sit-stand-underframe-corner-table-white-40263221/) $480CAD and husband made a top out of a sheet of plywood and melamine paint 2020-11-20
 - 3 [NB F100A Gas Spring Arm 22-35 inch Screen Monitor Holder 360 Rotate Tilt Swivel Desktop Monitor Mount Arm with Two USB3.0 Ports](https://www.aliexpress.com/item/32867957926.html?gps-id=pcStoreJustForYou&scm=1007.23125.137358.0&scm_id=1007.23125.137358.0&scm-url=1007.23125.137358.0&pvid=2d630778-8bed-40f9-939d-33449f8e5f65&spm=a2g0o.store_home.smartJustForYou_98366373.1) $27.37 CAD each 2020-11-20
 - [Herman Miller Aeron Chair](https://store.hermanmiller.com/office/office-chairs/aeron-chair/2195348.html?lang=en_CA#lang=en_CA&start=1) $1,545 CAD 2020-11-20
 - [impacto.ca x-standing mat](https://www.impacto.ca/products/medium-standing-mat/) $60 CAD 2020-11-20

## On the road

 - AOC 16" USB powered LED Monitor E1649FWU $99USD 2019-05-01 - died 2020-11-30

 ## Software

 - https://krisp.ai/ use https://ref.krisp.ai/u/u3d08078b9 Simply share this link in slack, discord, twitter etc., and let friends get 
2 months free Krisp once they install it.
 - Chrome
 - Slack
 
 ## Wish List
